<?php

namespace Drupal\upgrade_plan\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Render\Markup;

/**
 * Controller routines for upgrade plan page.
 */
class UpgradePlanController extends ControllerBase {

  /**
   * Render table of upgrade plan.
   */
  public function upgradePlanPage(): array {
    $rows = [
      [Markup::create('<strong>Package 1</strong>'), 'New Package'],
      [Markup::create('<s>Package 2</s>'), 'New Package'],
      [Markup::create('<div>Package 3</div>'), 'New Package'],
    ];
    $header = [
      'title' => t('Package Name'),
      'content' => t('Package Status'),
    ];
    $build['table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => t('No content has been found.'),
    ];
    return $build;
  }

}
