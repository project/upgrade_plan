<?php

namespace Drupal\Tests\upgrade_plan\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Test project navigation.
 *
 * @group upgrade_plan
 */
class UpgradePlanPageTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['upgrade_plan'];

  /**
   * The installation profile to use with this test.
   *
   * We need the 'minimal' profile in order to make sure the Tool block is
   * available.
   *
   * @var string
   */
  protected $profile = 'minimal';

  /**
   * User object for our test.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $webUser;

  /**
   * Stores an admin user used by the different tests.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * Verify that current user has no access to page.
   *
   * @param string $url
   *   URL to verify.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function upgradePlanPageVerifyNoAccess(string $url): void {
    // Test that page returns 403 Access Denied.
    $this->drupalGet($url);
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Test of the existence of upgrade plan link on admin page.
   */
  public function testUpgradePlanAdminLinks(): void {
    $assert = $this->assertSession();

    $this->adminUser = $this->drupalCreateUser([
      'access upgrade plan page',
      'access administration pages',
      'access site reports',
    ]);
    $this->drupalLogin($this->adminUser);

    // Test the upgrade plan page.
    $this->drupalGet('admin');
    $assert->linkExists('Reports');
    $this->click('[href*="admin/reports"]');
    $assert->linkExists('Upgrade Plan');
    $this->click('[href*="admin/reports/upgrade-plan"]');
    $assert->pageTextContains('PACKAGE NAME');
  }

  /**
   * Test of the existence of upgrade plan config link on module page.
   */
  public function testUpgradePlanConfigLinks(): void {
    $assert = $this->assertSession();

    $this->adminUser = $this->drupalCreateUser([
      'access upgrade plan page',
      'access administration pages',
      'administer modules',
    ]);
    $this->drupalLogin($this->adminUser);

    // Test the upgrade plan page.
    $this->drupalGet('admin/modules');
    $assert->statusCodeEquals(200);

    // Assert that the config link registered by upgrade_plan is present.
    $assert->elementExists('css', 'form.system-modules [href*="admin/reports/upgrade-plan"]');
    $this->click('[href*="admin/reports/upgrade-plan"]');
    $assert->pageTextContains('PACKAGE NAME');
  }

  /**
   * Main test.
   *
   * Login user and test page functionality through
   * the admin and user interfaces.
   */
  public function testUpgradePlanPage(): void {
    $assert_session = $this->assertSession();
    // Verify that anonymous user can't access the pages.
    $this->upgradePlanPageVerifyNoAccess('admin/reports/upgrade-plan');
    $assert_session->pageTextNotContains('PACKAGE NAME');

    // Create a regular user and login.
    $this->webUser = $this->drupalCreateUser();
    $this->drupalLogin($this->webUser);

    // Verify that regular user can't access the pages.
    $this->upgradePlanPageVerifyNoAccess('admin/reports/upgrade-plan');
    $assert_session->pageTextNotContains('PACKAGE NAME');

    // Create a user with permissions to access 'simple' page and login.
    $this->webUser = $this->drupalCreateUser(['access upgrade plan page']);
    $this->drupalLogin($this->webUser);

    // Verify that user can access simple content.
    $this->drupalGet('/admin/reports/upgrade-plan');
    $assert_session->statusCodeEquals(200);
    $assert_session->pageTextContains('PACKAGE NAME');
  }

}
